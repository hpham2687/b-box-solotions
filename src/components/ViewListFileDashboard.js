import React, { Component } from "react";
import { Table, Card , Modal, Button, Form } from 'antd';

import SelectComponent from "./SelectComponent";
import CascaderComponent from "./CascaderComponent";
import SelectByTextComponent from "./SelectByTextComponent";

const columnsTabListSharedFile = [
  {
    title: "File Name",
    dataIndex: "name",
    key: "name",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "transaction",
    dataIndex: "transaction",
    key: "transaction",
  },
  {
    title: "type",
    dataIndex: "type",
    key: "type",
  },
  {
    title: "Action",
    key: "action",
    render: (text, record) => (
          <span>
        {/* <a style={{ marginRight: 16 }}>Invite {record.name}</a> */}
          <a>Delete</a>
      </span>
    ),
  },
];

const tabList = [
  {
    key: "tabListOwnFile",
    tab: "tabListOwnFile",
  },
  {
    key: "tabListOrgFile",
    tab: "tabListOrgFile",
  },
  {
    key: "tabListSharedFile",
    tab: "tabListSharedFile",
  },
];

class ListOwnFileTable extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    bottom: "bottomRight",
    key: "tabListOwnFile",
    dataToSource: [
      { key: 0, name: "lg.png", owner: "_Bh46nEBRo4DRy0NWfcj", type: "png" },
    ],
    data2SourceOwnFile: [],
    data2SourceSharedFile: [
      {
        key: 3,
        name: "data2SourceSharedFile",
        transaction: "dvdvdv",
        type: "dpng",
      },
    ],
    visibleModalShareOwnFile: false,
    selectingFile2ShareModal: [],
    data2SendShareOwnFile: {
      shareTo: "ORG",
      orgName: "BACKKHOA",
      username: null,
      version: 1,
      cid: null,
      file_id: null,
      list_attribute: "sinhvien,thayco,hieutruong",
      isUsePassSecret: false,
      passwordSecret: "default",
      privateKey: "default",
      isUseBlockchain: false,
    },
  };

  showModalShareOwnFile = async (e) => {
    let rawData = this.props.listOwnFile.data
    let a = {...this.state.data2SendShareOwnFile, file_id: rawData[e]._id}
    this.setState({
      data2SendShareOwnFile: a,
      selectingFile2ShareModal: this.state.data2SourceOwnFile[e],
      visibleModalShareOwnFile: true,
    });
  };

  handleSubmitShareOwnFile = (e) => {
    
    this.props.shareFileEccDispatch(this.state.data2SendShareOwnFile)
    this.setState({
      visibleModalShareOwnFile: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    let data2SendShareOwnFileDefault = {
      shareTo: "ORG",
      orgName: "BACKKHOA",
      version: 1,
      list_attribute: "sinhvien,thayco,hieutruong",
      isUsePassSecret: false,
      passwordSecret: "DEFAULT",
      isUseBlockchain: false,
    };

    this.setState({
      visibleModalShareOwnFile: false,
      data2SendShareOwnFile: data2SendShareOwnFileDefault,
      selectingFile2ShareModal: null
    });
    this.props.resetListAttributesOrgDispatch();
  };

  columns = [
    {
      title: "File Name",
      dataIndex: "name",
      key: "name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "owner",
      dataIndex: "owner",
      key: "owner",
    },
    {
      title: "type",
      dataIndex: "type",
      key: "type",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record, index) => (
          <span>
          {/* <a style={{ marginRight: 16 }}>Invite {record.name}</a> */}
          <Button type="primary" onClick={(e)=>this.showModalShareOwnFile(index)}>
              Share 
        </Button>
          <a>Delete</a>
        </span>
      ),
    },
    {
      title: "",
      dataIndex: "_id",
      key: "_id",
      width: "0%"
    },
  ];

  onTabChange = (key, type) => {
    console.log(key, type);
    this.setState({ [type]: key });
  };

  componentDidMount() {
    console.log("didmount");
    this.props.fetchListOwnFileDispatch();
    // console.log(this.props.listOwnFile)
  }

  shouldComponentUpdate(nextProps, nextState) {
    // handle sau khi fetch list own file, store change, cap nhat table
    if (this.props.listOwnFile !== nextProps.listOwnFile) {
      if (nextProps.listOwnFile) {
        let data2SourceOwnFile = [];
        var data2Process = nextProps.listOwnFile.data;
        console.log(data2Process);
        if (data2Process)
          data2Process.map((value, index) => {
            value = value._source;
            data2SourceOwnFile.push({
              key: index,
              name: value.file_name,
              owner: value.owner,
              type: value.type,
              file_history: value.file_history,
            });
            return 0;
          });
        console.log(data2SourceOwnFile);
        this.setState({data2SourceOwnFile})
      }
      return false;
    }
    if (this.props.listSharedFile !== nextProps.listSharedFile) {
      if (nextProps.listSharedFile) {
        let data2SourceSharedFile = [];
        var data2Process = nextProps.listSharedFile.data;
        data2Process.map((value, index) => {
          data2SourceSharedFile.push({
            key: index,
            name: value.file_name,
            transaction: value.transaction,
            type: value.type,
          });
          return 0;
        });
        console.log(data2SourceSharedFile);
        this.setState({data2SourceSharedFile})
      }
      return false;
    }
    return true;
  }

  renderContentList() {
    const contentList = {
      tabListOwnFile: (
        <Table
          columns={this.columns}
          pagination={{ position: [this.state.bottom] }}
          dataSource={this.state.data2SourceOwnFile}
        />),
      tabListOrgFile: (<Table
          columns={this.columns}
          pagination={{ position: [this.state.bottom] }}
          dataSource={this.state.data2SourceOrgFile}
        />
      ),
      tabListSharedFile: (<Table
          columns={columnsTabListSharedFile}
          pagination={{ position: [this.state.bottom] }}
          dataSource={this.state.data2SourceSharedFile}
        />
      ),
    };

    return contentList[this.state.key];
  }

  handleOnTabChange(key) {
    this.onTabChange(key, "key");
    if (key === "tabListOrgFile") {
      this.props.fetchListOrgFileDispatch();
    }
    if (key === "tabListSharedFile") {
      this.props.fetchListSharedFileDispatch();
    }
  }

  handleChangeValueObjectSelectShareOwnFile = (e) => {
    if (e.type === "shareTo"){
      let data2SendShareOwnFile = {
        ...this.state.data2SendShareOwnFile,
        shareTo: e.payload,
      };
      this.setState({ ...this.state, data2SendShareOwnFile });
    }
    if (e.type === "version"){
      console.log(e.payload)
      let data2SendShareOwnFile = {
        ...this.state.data2SendShareOwnFile,
        cid: e.payload,
      };
      this.setState({ ...this.state, data2SendShareOwnFile });
    }
    if (e.type === "private_key"){
      let data2SendShareOwnFile = {
        ...this.state.data2SendShareOwnFile,
        privateKey: e.payload,
      };
      this.setState({ ...this.state, data2SendShareOwnFile });
    }
    if (e.type === "username"){
      let data2SendShareOwnFile = {
        ...this.state.data2SendShareOwnFile,
        username: e.payload,
      };
      this.setState({ ...this.state, data2SendShareOwnFile });
    }
    if (e.type === "orgName"){
      let data2SendShareOwnFile = {
        ...this.state.data2SendShareOwnFile,
        orgName: e.payload,
      };
      this.setState({ ...this.state, data2SendShareOwnFile });
    }

  };
  render() {
    let {listAttributesOrg} = this.props;
    let {visibleModalShareOwnFile, data2SendShareOwnFile, selectingFile2ShareModal} = this.state;
    return (
      <>
        <Modal
                title="Modal Share Own File"
                visible={visibleModalShareOwnFile}
                onOk={this.handleSubmitShareOwnFile}
                onCancel={this.handleCancel}
              >
         
          <CascaderComponent value2Select={[{
                              value: 'ORG',
                              label: 'ORG',
                              isLeaf: false,
                            },
                            {
                              value: 'USER',
                              label: 'USER',
                              isLeaf: true,
                            }]}
                            dataSelectedFile={selectingFile2ShareModal}
                            handleChangeValueObjectSelectShareOwnFile={this.handleChangeValueObjectSelectShareOwnFile}
                            {...this.props}
                            />
            

            {(listAttributesOrg && data2SendShareOwnFile.shareTo === "ORG") && 
            <><Form.Item name="password_secret" label="Choose Attribute" rules={[{ required: true }]}>
           <SelectByTextComponent {...this.props}/>
                  
                  </Form.Item></>}
              </Modal>

              <Card
title="FILE MANAGER" bordered
          tabList={tabList}
          activeTabKey={this.state.key}
          onTabChange={key => this.handleOnTabChange(key)}
        >
          {this.renderContentList()}
        </Card>
      </>
    );
  }
}

export default ListOwnFileTable;
