import React, { Component } from 'react';
import {Animated} from "react-animated-css";

import {Form, Cascader, Input, Radio } from 'antd';
import SelectComponent from './SelectComponent';


class CascaderComponent extends Component {
    constructor(props){
        super(props)
    }
    state = {
        options: [],
        isShowInputUsername: false,
        valuePasswordSecret: 1,
        valueBlockchain: 1,

      };
      targetOption = null
      componentDidMount(){
        console.log(this.props.value2Select)
          this.setState({options: this.props.value2Select});
      }
      onChangeRadio = e => {
        
        //console.log('radio checked', e.target.name);
        if (e.target.name == "blockchain")
          this.setState({
            valueBlockchain: e.target.value,
          });
        else { // target name = pass secriet
          this.setState({
            valuePasswordSecret: e.target.value,
          });
        }
      };
      shouldComponentUpdate(nextProps, nextState){
          if (nextProps.listAllOrg !== this.props.listAllOrg){
             // alert('change')
            // console.log(nextProps.listAllOrg)
            this.targetOption.loading = false;
            let listData = nextProps.listAllOrg.data.map((value,index)=>{
                return {
                    label: value._source.org_name,
                    value: value._source.org_name
                }
            })
            this.targetOption.children = listData;
             

            return true
          }
          return true
      }
      onChange = async (value, selectedOptions) => {
        //console.log(value, value[1]);
      //  console.log(typeof(value[1])=="undefined")
        if (value[0] && (typeof(value[1])=="undefined") )
        if (value[0] == "ORG"){
        //  alert('vao day')
            this.props.handleChangeValueObjectSelectShareOwnFile({type:"shareTo", payload:"ORG"})
            this.targetOption = selectedOptions[selectedOptions.length - 1];
            //fetch org
            this.props.fetchListAllOrgDispatch()
            //console.log(this.props.listAllOrg)
            this.targetOption.loading = true;
            this.setState({isShowInputUsername: false});

            // fetchListAttributesOrgDispatch
        }else if (value[0] == "USER"){
          this.props.handleChangeValueObjectSelectShareOwnFile({type:"shareTo", payload:"USER"})
          this.setState({isShowInputUsername: true});
          return;
        }

        if (value[0] && (typeof(value[1])!=="undefined") ){ // chon cai tiep theo, value[1] la cai tiep
        // alert(value[1])
          let data2Send = value[1];
          this.props.handleChangeValueObjectSelectShareOwnFile({type:"orgName", payload:data2Send})
          this.props.fetchListAttributesOrgDispatch(data2Send) // send name of ORG
        }


      };
    
      loadData = selectedOptions => {
       // console.log(selectedOptions)
      
      };
      RenderInputUsername = () => {
        return (  
        <Animated animationIn="pulse" animationOut="bounce" animationInDuration={400} animationOutDuration={400} isVisible={true}>
                    <Form name="form-passSecret">

             <Form.Item name="username" label="Username" rules={[{ required: true }]}/>

               <Input name="username" onChange={(e)=>this.onChangeInput(e)} />
      </Form>
         </Animated>
    )
      }
      onChangeInput = e => {
        let name = e.target.name;
        console.log(e.target.name)
        if (name === "username"){
          this.props.handleChangeValueObjectSelectShareOwnFile({type:"username", payload:e.target.value})
        }else if (name === "private_key"){
          this.props.handleChangeValueObjectSelectShareOwnFile({type:"private_key", payload:e.target.value})

        }

//        

      }
    
    render() {
        return (
            <>
               <Cascader
        options={this.state.options}
        loadData={this.loadData}
        onChange={this.onChange}
        changeOnSelect
      />   
      <SelectComponent  {...this.props} value2Select={this.props.dataSelectedFile.file_history}/>
      
        {this.state.isShowInputUsername && <this.RenderInputUsername/>}

        <Form name="form-passSecret">

<Form.Item name="password_secret" label="Password Secret" rules={[{ required: true }]}>
<Radio.Group onChange={(e)=> this.onChangeRadio(e)} defaultChecked="true" defaultValue={1} value={this.state.valuePasswordSecret}>
        <Radio value={1}>Use</Radio>
        <Radio value={2}>Not Use</Radio>
        
      </Radio.Group>
      
      </Form.Item>
      {this.state.valuePasswordSecret === 2 && <Input name="username" onChange={(e)=>this.onChangeInput(e)} />}

      <Form.Item name="useBlockchain" label="Use Blockchain" rules={[{ required: true }]}>
<Radio.Group name="blockchain" onChange={(e)=>this.onChangeRadio(e)} defaultChecked="true" defaultValue={1} value={this.state.valueBlockchain}>
        <Radio value={1}>Use</Radio>
        <Radio value={2}>Not Use</Radio>
        
      </Radio.Group>
      
      </Form.Item>

      {this.state.valueBlockchain === 2 && <Input name="private_key" onChange={(e)=>this.onChangeInput(e)} />}

      </Form>
            </>
        );  
    }
}

export default CascaderComponent;