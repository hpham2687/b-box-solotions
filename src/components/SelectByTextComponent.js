import React, { Component } from 'react';
import { Select } from 'antd';

const { Option } = Select;

 

class SelectByTextComponent extends Component {
    state = {
        isOpen: true
    }
    handleChange(value) {
        console.log(`selected ${value}`);
      }
    renderListValue() {
    return this.props.listAttributesOrg.data.map((value, index) => {
        return (
        <Option key={index} value={value._source.attrs_name}>
            {value._source.attrs_name}
        </Option>
        );
    });
    }
    handleMouseLeave = () => {
        // this.setState({
        //     isOpen:!this.state.isOpen
        // });
    }
    render() {
        let children = []
        children = this.renderListValue()
        return (
            <>
            <Select autoFocus="true" defaultOpen={this.state.isOpen}  mode="tags" style={{ width: '100%' }}
             onChange={this.handleChange}
              tokenSeparators={[',']}>
                {children}
            </Select>
            
            </>
        );
    }
}

export default SelectByTextComponent;