import React, { Component } from "react";
import { Layout, Menu } from "antd";

import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import * as typesUi from "../constants/ui";

const { Header, Sider, Content } = Layout;

class SiderBarComponent extends Component {
  RenderLoggedIn = () => {
    const { userData } = this.props;
    return (
      <>
        <Sider
          theme="light"
          trigger={null}
          collapsible
          collapsed={this.props.collapsed}
        >
          <div className="logo">
            <img
                style={{ height: "50px" }}
                src="https://res.cloudinary.com/djy5gktft/image/upload/v1573179162/BKC/logovchain_wqwzis.png"
              />
          </div>
          <Menu
            theme="light"
            mode="inline"
            selectedKeys={[`${this.props.activeItemSideBar}`]}>
            <Menu.Item
                onClick={() => {
                this.props.setActiveItemSidebar(1);
                this.props.setCurrentViewDashboardDisPatch(
                  typesUi.VIEW_LIST_FILE_DASHBOARD
                );
              }}
                key="1"
                icon={<UserOutlined />}
              >
                <Link>{userData.data._source.email || "user"}</Link>
              </Menu.Item>
            <Menu.Item
                onClick={() => {
                this.props.setActiveItemSidebar(2);
                this.props.setCurrentViewDashboardDisPatch(
                  typesUi.VIEW_PROFILE_DASHBOARD
                );
              }}
                key="2"
                icon={<VideoCameraOutlined />}
              >
              PROFILE SETTING
              </Menu.Item>
            <Menu.Item
                onClick={() => {
                this.props.setActiveItemSidebar(3);
                this.props.setCurrentViewDashboardDisPatch(
                  typesUi.VIEW_UPLOAD_DASHBOARD
                );
              }}
                key="3"
                icon={<VideoCameraOutlined />}
              >
              UPLOAD
              </Menu.Item>
            <Menu.Item
                onClick={() => this.props.setIsLoggedInDispatch(false)}
                key="4"
                icon={<UploadOutlined />}
              >
              LOG OUT
              </Menu.Item>
          </Menu>
        </Sider>
      </>
    );
  };

  RenderLoggedOut = () => {
    return (
      <>
        <Sider
          theme="light"
          trigger={null}
          collapsible
          collapsed={this.props.collapsed}
        >
          <div className="logo">
            <img
              style={{ height: "50px" }}
              src="https://res.cloudinary.com/djy5gktft/image/upload/v1573179162/BKC/logovchain_wqwzis.png"
            />
          </div>
          <Menu
            theme="light"
            mode="inline"
            selectedKeys={[`${this.props.activeItemSideBar}`]}>
            <Menu.Item
                onClick={() => this.props.setActiveItemSidebar(1)}
                key="1"
                icon={<UserOutlined />}
              >
                <Link to="/">HOME</Link>
              </Menu.Item>
            <Menu.Item
                onClick={() => this.props.setActiveItemSidebar(2)}
                key="2"
                icon={<VideoCameraOutlined />}
              >
              Sign In
              </Menu.Item>
            <Menu.Item
                onClick={() => this.props.setActiveItemSidebar(3)}
                key="3"
                icon={<UploadOutlined />}
              >
              Sign Up
              </Menu.Item>
          </Menu>
        </Sider>
      </>
    );
  };

  render() {
    console.log(this.props.userData);
    if (
      this.props.isLoggedIn &&
      this.props.userData &&
      this.props.userData.data
    )
      return <this.RenderLoggedIn />;
    return <this.RenderLoggedOut />;
  }
}

export default SiderBarComponent;
