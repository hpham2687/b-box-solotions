import React, { Component } from "react";
import { Select } from "antd";

const { Option } = Select;

class SelectComponent extends Component {
  constructor(props) {
    super(props);
  }

  onBlur() {
    console.log("blur");
  }

  onFocus() {
    console.log("focus");
  }

  onSearch(val) {
    console.log("search:", val);
  }

  renderListValue() {
    return this.props.value2Select.map((value, index) => {
      return (
        <Option key={index} value={index.cid}>
          {index}
        </Option>
      );
    });
  }

  onChange = (value) => {
    console.log(`selected ${value}`);
    this.props.handleChangeValueObjectSelectShareOwnFile({type:"version", payload:value})

  };

  render() {
    return (
      <>
        <Select 
          showSearch
          style={{ width: 200,marginLeft:'10px' }}
          placeholder="Select a verison"
          optionFilterProp="children"
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onSearch={this.onSearch}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {this.renderListValue()}
        </Select>
      </>
    );
  }
}

export default SelectComponent;
