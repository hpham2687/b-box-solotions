import React, { Component } from "react";
import { Form, Input, Button, Checkbox, Typography, Card } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

class SignInForm extends Component {
  state = {
    data2Send: {
      email: "",
      password: "",
    },
  };

  layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 6 },
  };

  tailLayout = {
    wrapperCol: { offset: 4, span: 6 },
  };

  handleChange(e) {
    const data2Send = {
      ...this.state.data2Send,
      [e.target.name]: e.target.value,
    };
    this.setState({ data2Send });
  }

  handleSubmit() {
    console.log(this.state.data2Send);
    const data2Send = new FormData(); // creat data to send
    data2Send.append("email", this.state.data2Send.email);
    data2Send.append("password", this.state.data2Send.password);
    this.props.loginDispatch(data2Send);
  }

  render() {
    const { Title } = Typography;
    return (
      <>
        <div className="site-card-border-less-wrapper">
          <Card title="LOGIN FORM" bordered>
            <Form
              name="normal_login"
              className="login-form"
              initialValues={{ remember: true }}
            >
              <Form.Item
                rules={[
                  { required: true, message: "Please input your Username!" },
                ]}
              >
                <Input
                  onChange={(e) => this.handleChange(e)}
                  name="email"
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Username"
                />
              </Form.Item>
              <Form.Item
                onChange={(e) => this.handleChange(e)}
                rules={[
                  { required: true, message: "Please input your Password!" },
                ]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  name="password"
                  onChange={(e) => this.handleChange(e)}
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                  <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <a className="login-form-forgot" href="">
                  Forgot password
                </a>
              </Form.Item>

              <Form.Item>
                <Button
                  onClick={() => this.handleSubmit()}
                  type="primary"
                  htmlType="submit"
                  className="login-form-button btn-full-width"
                >
                  Log in
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </div>
      </>
    );
  }
}

export default SignInForm;
