import React, { Component } from "react";
import { Form, Card, Row, Button, Col, Input } from "antd";
import { Avatar } from 'antd';

class ViewProfileDashBoard extends Component {
  render() {
    const { userData } = this.props;
    return (
         <>
              <Card
                title="Profile Setting"
                extra={
                <Button
                size="small"
                style={{ margin: '0 16px', verticalAlign: 'middle' }}
                >
                ChangeUser
                </Button>} 
        
                bordered={true}
        >
          <Row>
             <Col md={4}>
                  <Avatar style={{ backgroundColor: '#f56a00', verticalAlign: 'middle' }} size="large" gap={4}>
                      U
                        </Avatar>
              <Button
                size="small"
                          style={{ margin: '0 16px', verticalAlign: 'middle' }}
              >
                          ChangeUser
                        </Button>
            </Col>
             <Col md={18}>
              <Row>
                <Col span={10}>
                  <Form.Item label="Username">
                          <Input disabled value={userData.data._source.username} />
                  </Form.Item>
                  <Form.Item label="Adress">
                    <Input disabled value={userData.data._source.address} />
                  </Form.Item>
                                </Col>
                <Col offset={4} span={10}>
                  <Form.Item label="Email">
                      <Input disabled value={userData.data._source.email} />
                  </Form.Item>
                  <Form.Item label="Role">
                      <Input
                      disabled
                      value={
                        userData.data._source.email == 1 ? "Admin" : "User"
                      }
                    />
                  </Form.Item>
                        </Col>
              </Row>
            </Col>

             <Form.Item>
              <Button type="primary">Change Password</Button>
            </Form.Item>
          </Row>
          <Row>
             <Form
             layout="inline"
           >
              <Form.Item label="Old Pass">
                <Input placeholder="input placeholder" />
              </Form.Item>
              <Form.Item label="New Pass">
                <Input placeholder="input placeholder" />
              </Form.Item>

              <Form.Item label="Confirm Pass">
                <Input placeholder="input placeholder" />
              </Form.Item>
            </Form>
          </Row>
        </Card>
      </>
    );
  }
}

export default ViewProfileDashBoard;
