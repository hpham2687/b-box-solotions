import axiosClient from "./axiosClient";

class FetchFileAPIs {
  localStorage() {
    return localStorage.userToken;
  }


shareFileECC = (payload) => {
  
  let data2Send = new FormData();

  const config = {
    headers: {
      authorization: localStorage.userToken,
    },
  };

  let url = "/user/shareFileForUser";
  if (payload.shareTo === "ORG"){
    url = "/user/shareFileForOrg";
    data2Send.append("org", payload.orgName);
    data2Send.append("attrs_user", payload.list_attribute);

    

  }


  data2Send.append("password_aes", payload.passwordSecret);
  data2Send.append("privateKey", payload.privateKey);
  
  data2Send.append("email_share", payload.username);

  data2Send.append("cid", payload.cid);
  data2Send.append("file_id", payload.file_id);
 

  console.log(payload)


  return axiosClient.post(url, data2Send, config);
};



}

const FetchFileAPI = new FetchFileAPIs();
export default FetchFileAPI;
