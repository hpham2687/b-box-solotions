import axiosClient from "./axiosClient";

class FetchFileAPIs {
  localStorage() {
    return localStorage.userToken;
  }

  getUserProfile = (data2Send) => {
    const config = {
      headers: {
        authorization: this.localStorage(),
      },
    };
    // console.log(this.config)
    //  console.log('gui di :`'+this.config.headers.authorization+'`')
    //  console.log('gui di log :`'+localStorage.+'`')
    const url = "/user/profile";
    return axiosClient.get(url, config);
  };

  getListOwnFile = (data2Send) => {
    const config = {
      headers: {
        authorization: localStorage.userToken,
      },
    };
    const url = "/user/list_file";
    return axiosClient.get(url, config);
  };

  getListOrgFile = (data2Send) => {
    const config = {
      headers: {
        authorization: localStorage.userToken,
      },
    };
    const url = "/user/getListFileOrg";
    return axiosClient.get(url, config);
  };

  getListSharedFile = (data2Send) => {
    const config = {
      headers: {
        authorization: localStorage.userToken,
      },
    };
    const url = "/user/getShare";
    return axiosClient.get(url, config);
  };


getAllOrg = (data2Send) => {
  const config = {
    headers: {
      authorization: localStorage.userToken,
    },
  };
  const url = "/user/getAllOrg";
  return axiosClient.get(url, config);
};

getAttributesOrg = (payload) => {
  let data2Send = new FormData();
  console.log(payload)
  data2Send.append("org", payload);

  const config = {
    headers: {
      authorization: localStorage.userToken,
    },
  };
  const url = "/user/getAttrs";
  return axiosClient.post(url, data2Send, config);
};



}

const FetchFileAPI = new FetchFileAPIs();
export default FetchFileAPI;
