import { put, takeEvery, call, delay, select } from "redux-saga/effects";
import AuthAPI from "../api/authApi";
import FetchAPI from "../api/fetchFileApi";
import * as types from "../constants/user";
import * as typesUi from "../constants/ui";

// import { history } from './../App';
import { openNotificationWithIcon } from "../actions/ui";

function* login(action) {
  try {
    const response = yield AuthAPI.login(action.payload);
    console.log(response);
    if (response.status == 200) {
      localStorage.userToken = response.token;

      yield put({ type: types.SET_IS_LOGGED_IN, payload: true });
      yield put({ type: types.SIGNIN_USER_SUCCESS, payload: response });
      openNotificationWithIcon(
        typesUi.NOTIFICATION_SUCCESS,
        "LOGIN SUCCESS",
        response.msg
      );
    } else if (response.status == 500) {
      yield put({ type: types.SIGNIN_USER_FAILURE, payload: response.msg });
      openNotificationWithIcon(
        typesUi.NOTIFICATION_ERROR,
        "LOGIN ERROR",
        response.msg
      );
    }
    //   console.log(localStorage.userToken)
  } catch (err) {
    yield put({ type: types.SIGNIN_USER_FAILURE, payload: err });
    openNotificationWithIcon(typesUi.NOTIFICATION_ERROR, err);
  }
}

function* getListOwnFile(action) {
  try {
    const response = yield FetchAPI.getListOwnFile();
    console.log(response);
    yield put({
      type: types.GET_LIST_OWN_FILE_USER_SUCCESS,
      payload: response,
    });
    if (response.status == 200) {
    }
  } catch (err) {
    yield put({ type: types.GET_LIST_OWN_FILE_USER_FAILURE, payload: err });
  }
}

function* getListOrgFile(action) {
  try {
    const response = yield FetchAPI.getListOrgFile();
    console.log(response);
    yield put({
      type: types.GET_LIST_ORG_FILE_USER_SUCCESS,
      payload: response,
    });
    if (response.status == 200) {
    }
  } catch (err) {
    yield put({ type: types.GET_LIST_ORG_FILE_USER_FAILURE, payload: err });
  }
}

function* getListAllOrg(action) {
  try {
    const response = yield FetchAPI.getAllOrg();
    console.log(response);
    yield put({
      type: types.GET_ALL_ORG_SUCCESS,
      payload: response,
    });
    if (response.status == 200) {
    }
  } catch (err) {
    yield put({ type: types.GET_ALL_ORG_FAILURE, payload: err });
  }
}


function* getListAttributesOrg(action) {
  try {
    const response = yield FetchAPI.getAttributesOrg(action.payload);
    console.log(response);
    yield put({
      type: types.GET_ATTRIBUTES_ORG_SUCCESS,
      payload: response,
    });
    if (response.status == 200) {
    }
  } catch (err) {
    yield put({ type: types.GET_ATTRIBUTES_ORG_FAILURE, payload: err });
  }
}

function* getListSharedFile(action) {
  try {
    const response = yield FetchAPI.getListSharedFile();
    console.log(response);
    yield put({
      type: types.GET_LIST_SHARED_FILE_USER_SUCCESS,
      payload: response,
    });
    if (response.status == 200) {
    }
  } catch (err) {
    yield put({ type: types.GET_LIST_SHARED_FILE_USER_FAILURE, payload: err });
  }
}

function* getUserProfile(action) {
  try {
   // console.log(localStorage.userToken);
    const response = yield FetchAPI.getUserProfile();
   // console.log(response);
    if (response.msg === "Token is invalid") {
      alert(`invalid${localStorage.userToken}`);
      yield put({ type: types.SET_IS_LOGGED_IN, payload: false });
      return;
    }
    const isLoggedIn = yield select((state) => state.user.isLoggedIn);
    if (!isLoggedIn) {
      yield put({ type: types.SET_IS_LOGGED_IN, payload: true });

      console.log(localStorage.userToken);
    }
    yield put({ type: types.GET_PROFILE_USER_SUCCESS, payload: response });
  } catch (err) {
    yield put({ type: types.GET_PROFILE_USER_FAILURE, payload: err });
  }
}

export const userSaga = [
  takeEvery(types.SIGNIN_USER_REQUEST, login),
  takeEvery(types.GET_LIST_OWN_FILE_USER_REQUEST, getListOwnFile),
  takeEvery(types.GET_LIST_ORG_FILE_USER_REQUEST, getListOrgFile),
  takeEvery(types.GET_LIST_SHARED_FILE_USER_REQUEST, getListSharedFile),
  takeEvery(types.GET_PROFILE_USER_REQUEST, getUserProfile),
  takeEvery(types.GET_ALL_ORG_REQUEST, getListAllOrg),
  takeEvery(types.GET_ATTRIBUTES_ORG_REQUEST, getListAttributesOrg),

  
];


