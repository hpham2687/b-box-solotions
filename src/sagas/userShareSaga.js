
import { put, takeEvery, call, delay, select } from "redux-saga/effects";
import shareFileApi from "../api/shareFileApi";
import * as types from "../constants/user";
import * as typesUi from "../constants/ui";
import { openNotificationWithIcon } from "./../actions/ui";



function* shareFileECC(action) {
    try {
      const response = yield shareFileApi.shareFileECC(action.payload);
      console.log(response);
    //   yield put({
    //     type: types.GET_LIST_OWN_FILE_USER_SUCCESS,
    //     payload: response,
    //   });
      if (response.msg === "Share success") {
        openNotificationWithIcon(
            "success",
            "Share success",            
          );
      }
    } catch (err) {
      yield put({ type: types.GET_LIST_OWN_FILE_USER_FAILURE, payload: err });
    }
  }

export const userShareSaga = [
    takeEvery(types.SHARE_FILE_ECC_USER_REQUEST, shareFileECC),
];