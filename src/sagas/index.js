import { all } from "redux-saga/effects";
import { userSaga } from "./userSaga";
import { userShareSaga } from "./userShareSaga";

function* rootSaga() {
  yield all([...userSaga, ...userShareSaga]);
}

export default rootSaga;
