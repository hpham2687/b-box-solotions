import * as types from "../constants/ui";

const DEFAULT_STATE = {
  collapsedSideBar: false,
  activeItemSideBar: 1,
  currentViewDashboard: types.VIEW_LIST_FILE_DASHBOARD,
};
export default (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case types.SET_COLLAPSED_SIDEBAR:
      return {
        ...state,
        collapsedSideBar: action.payload,
      };
    case types.SET_ACTIVE_ITEM_SIDEBAR:
      return {
        ...state,
        activeItemSideBar: action.payload,
      };
    case types.SET_CURRENT_VIEW_DASHBOARD:
      return {
        ...state,
        currentViewDashboard: action.payload,
      };
    default:
      return state;
  }
};
