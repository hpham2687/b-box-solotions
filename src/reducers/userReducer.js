import * as types from "../constants/user";

const DEFAULT_STATE = {
  isLoggedIn: false,
  userData: null,
  listOwnFile: null,
  listOrgFile: null,
  listSharedFile: null,
  isFetching: false,
  listAllOrg: null,
  listAttributesOrg: null
};
export default (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case types.SET_IS_LOGGED_IN:
      return {
        ...state,
        isLoggedIn: action.payload,
        isFetching: false
      };
    case types.SIGNIN_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.SIGNIN_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
      };
    case types.SIGNIN_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case types.GET_LIST_OWN_FILE_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_LIST_OWN_FILE_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        listOwnFile: action.payload,
      };
    
    case types.GET_LIST_OWN_FILE_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    case types.GET_LIST_ORG_FILE_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_LIST_ORG_FILE_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        listOrgFile: action.payload,
      };
    case types.GET_LIST_ORG_FILE_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    case types.GET_LIST_SHARED_FILE_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_LIST_SHARED_FILE_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        listSharedFile: action.payload,
      };
    case types.GET_LIST_SHARED_FILE_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    case types.GET_PROFILE_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_PROFILE_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        userData: action.payload,
      };
    case types.GET_PROFILE_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

      case types.GET_ALL_ORG_REQUEST:
        return {
          ...state,
          isFetching: true,
        };
      case types.GET_ALL_ORG_SUCCESS:
        return {
          ...state,
          isFetching: false,
          listAllOrg: action.payload,
        };
      case types.GET_ALL_ORG_FAILURE:
        return {
          ...state,
          isFetching: false,
        };

      case 'RESET_LIST_ATTRIBUTES':
        return {
          ...state,
          listAttributesOrg: action.payload
        }
        case types.GET_ATTRIBUTES_ORG_REQUEST:
          return {
            ...state,
            isFetching: true,
          };
        case types.GET_ATTRIBUTES_ORG_SUCCESS:
          return {
            ...state,
            isFetching: false,
            listAttributesOrg: action.payload,
          };
        case types.GET_ATTRIBUTES_ORG_FAILURE:
          return {
            ...state,
            isFetching: false,
          };

    default:
      return state;
  }
};
