import { combineReducers } from "redux";
import uiReducer from "./uiReducer";
import userReducer from "./userReducer";
import userShareReducer from "./userShareReducer";

export default combineReducers({
  ui: uiReducer,
  user: userReducer,
  userShare: userShareReducer,
});
