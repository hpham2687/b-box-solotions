import * as types from "../constants/user";

const DEFAULT_STATE = {
  isSharing: false,
  message: null
};
export default (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case types.SHARE_FILE_ECC_USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.SHARE_FILE_ECC_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
      };
    
    case types.SHARE_FILE_ECC_USER_FAILURE:
      return {
        ...state,
        isSharing: false,
        message: action.payload
      };

    default:
      return state;
  }
};
