import * as types from "../../constants/user";


export function shareFileEccAction(payload) {
  return {
    type: types.SHARE_FILE_ECC_USER_REQUEST,
    payload,
  };
}

export function loginAction(payload) {
  return {
    type: types.SIGNIN_USER_REQUEST,
    payload,
  };
}


export function getListAttributesOrgAction(payload) {
  return {
    type: types.GET_ATTRIBUTES_ORG_REQUEST,
    payload,
  };
}



export function getListAllOrgAction() {
  return {
    type: types.GET_ALL_ORG_REQUEST,
  };
}

export function getListSharedFileAction() {
  return {
    type: types.GET_LIST_SHARED_FILE_USER_REQUEST,
  };
}

export function getListOwnFileAction() {
  return {
    type: types.GET_LIST_OWN_FILE_USER_REQUEST,
  };
}

export function getListOrgFileAction() {
  return {
    type: types.GET_LIST_ORG_FILE_USER_REQUEST,
  };
}

export function getUserProfileAction() {
  return {
    type: types.GET_PROFILE_USER_REQUEST,
  };
}
export function setIsLoggedIn(payload) {
  return {
    type: types.SET_IS_LOGGED_IN,
    payload,
  };
}
