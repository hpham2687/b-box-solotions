import { notification } from "antd";
import * as types from "../../constants/ui";

export function setCollapsedSidebar(payload) {
  return {
    type: types.SET_COLLAPSED_SIDEBAR,
    payload,
  };
}
export function setActiveItemSidebar(payload) {
  return {
    type: types.SET_ACTIVE_ITEM_SIDEBAR,
    payload,
  };
}
export function setCurrentViewDashboard(payload) {
  return {
    type: types.SET_CURRENT_VIEW_DASHBOARD,
    payload,
  };
}

export const openNotificationWithIcon = (type, title, content) => {
  notification[type]({
    message: title,
    description: content,
  });
};
