import React from "react";
import createHistory from "history/createBrowserHistory";
import "./App.css";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { connect } from "react-redux";
import SignIn from "./pages/SignIn";
import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import LoadingComponent from "./components/LoadingComponent";

function App(props) {
  const { isFetching } = props;
  // alert(isFetching)
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path="/signin"
            render={(props) => (
              <>{isFetching ? <LoadingComponent /> : <SignIn {...props} />}</>
            )}
          />
          <Route
            exact
            path="/"
            render={(props) => (
              <>
                <Home {...props} />}
              </>
            )}
          />
          <Route
            exact
            path="/dashboard"
            render={(props) => (
              <>
                <Dashboard {...props} />
              </>
            )}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

// const history = createHistory();
// export { history };

const mapStateToProps = (state) => {
  return {
    isFetching: state.user.isFetching,
  };
};
export default connect(mapStateToProps, null)(App);
