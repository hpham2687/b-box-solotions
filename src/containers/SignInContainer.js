import React, { Component } from "react";
import { Row, Col, Layout } from "antd";

import { connect } from "react-redux";
import SignInForm from "../components/SignInForm";
import * as actions from "../actions/user";

const { Content } = Layout;

class SignInContainer extends Component {
  render() {
    return (
      <>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <Row span={6} offset={8}>
            <Col span={6} offset={8}>
              <SignInForm {...this.props} />
            </Col>
          </Row>
        </Content>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginDispatch: (payload) => {
      dispatch(actions.loginAction(payload));
    },
  };
};
export default connect(null, mapDispatchToProps)(SignInContainer);
