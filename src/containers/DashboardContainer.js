import React, { Component } from "react";
import { Row, Col, Layout } from "antd";

import { connect } from "react-redux";
import * as actions from "../actions/ui";
import * as actionUser from "../actions/user";
import * as typesUi from "../constants/ui";

import ViewListFileDashboard from "../components/ViewListFileDashboard";
import ViewProfileDashBoard from "../components/ViewProfileDashBoard";

const { Content } = Layout;

class DashboardContainer extends Component {
  componentDidMount() {
    this.props.setActiveItemSidebar(1);
  }

  renderUploadView() {
    return "UPLOAD VIEW";
  }

  renderProfileView() {
    return (
      <>
        <Row span={12} offset={3}>         
          {/* ViewProfileDashBoard */}
          <Col span={14} offset={6}>
            <ViewProfileDashBoard {...this.props} />
          </Col>
        </Row>
        {/* ListOwnFileTable */}
      </>
    );
  }

  renderListFileView() {
    return (
      <>
        <Row span={12} offset={3}>
          {" "}
          {/* ViewListFileDashboard */}
          <Col span={14} offset={6}>
            <ViewListFileDashboard {...this.props} />
          </Col>
        </Row>
{" "}
        {/* ViewListFileDashboard */}
      </>
    );
  }

  render() {
    // console.log(this.props.listOwnFile)
    const { currentViewDashboard } = this.props;

    return (
      <>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          {currentViewDashboard == typesUi.VIEW_LIST_FILE_DASHBOARD &&
            this.renderListFileView()}
          {currentViewDashboard == typesUi.VIEW_PROFILE_DASHBOARD &&
            this.renderProfileView()}
          {currentViewDashboard == typesUi.VIEW_UPLOAD_DASHBOARD &&
            this.renderUploadView()}
        </Content>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listOwnFile: state.user.listOwnFile,
    listOrgFile: state.user.listOrgFile,
    listSharedFile: state.user.listSharedFile,
    currentViewDashboard: state.ui.currentViewDashboard,
    listAllOrg: state.user.listAllOrg,
    listAttributesOrg: state.user.listAttributesOrg
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchListSharedFileDispatch: () => {
      dispatch(actionUser.getListSharedFileAction());
    },
    fetchListOwnFileDispatch: () => {
      dispatch(actionUser.getListOwnFileAction());
    },
    fetchListAllOrgDispatch: () => {
      dispatch(actionUser.getListAllOrgAction());
    },
    fetchListAttributesOrgDispatch: (payload) => {
      dispatch(actionUser.getListAttributesOrgAction(payload));
    },
    resetListAttributesOrgDispatch: (payload) => {
      dispatch({ type: 'RESET_LIST_ATTRIBUTES', payload: null }      );
    },
    fetchListOrgFileDispatch: () => {
      dispatch(actionUser.getListOrgFileAction());
    },
    shareFileEccDispatch: (payload) => {
      dispatch(actionUser.shareFileEccAction(payload));
    },
    setActiveItemSidebar: (payload) => {
      dispatch(actions.setActiveItemSidebar(payload));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);
