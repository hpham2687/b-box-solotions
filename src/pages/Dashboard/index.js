import React, { Component } from "react";
import { Layout, Avatar } from 'antd';
import SiderBarComponent from '../../components/SiderBarComponent';

import * as actionsUser from "../../actions/user";
import * as actionsUi from "./../../actions/ui"
import { Redirect } from "react-router-dom";
import DashboardContainer from "./../../containers/DashboardContainer";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import { openNotificationWithIcon } from "./../../actions/ui";

const { Header } = Layout;

class Dashboard extends Component {
  componentDidMount() {
    this.props.fetchUserProfileDispatch();
  }

  toggle = () => {
    this.props.setCollapsedSidebarDispatch(!this.props.collapsed);
  };

  render() {
    //  alert(this.props.isLoggedIn)
    if (!this.props.isLoggedIn) {
      //  alert('not_logged')
      // alert('vao day')
      return <Redirect to="/signin" />;
    }
    // alert(this.props.isFetching)
    if (this.props.isLoggedIn)
      return (
          <>
          <Layout>
                  <SiderBarComponent {...this.props} />
            <Layout className="site-layout">
              <Header
                className="site-layout-background topbar-menu"
                style={{ padding: 0 }}
              >
                {React.createElement(
                  this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: this.toggle,
                  }
                )}
            <Avatar style={{float:'right'}} src="./logo192.png" />
              </Header>
          <DashboardContainer {...this.props} /> 
        </Layout>
                </Layout>  
        </>
      );
  }
}

const mapStateToProps = (state) => {
  return {
    collapsed: state.ui.collapsedSideBar,
    isLoggedIn: state.user.isLoggedIn,
    activeItemSideBar: state.ui.activeItemSideBar,
    userData: state.user.userData,
    isFetching: state.user.isFetching,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCollapsedSidebarDispatch: (payload) => {
      dispatch(actionsUi.setCollapsedSidebar(payload));
    },
    fetchUserProfileDispatch: () => {
      dispatch(actionsUser.getUserProfileAction());
    },
    setIsLoggedInDispatch: (payload) => {
      dispatch(actionsUser.setIsLoggedIn(payload));
      if (payload === false){
        openNotificationWithIcon(
          "success",
          "LOG OUT SUCCESS",
          "BAI BAI"
        );
      }
    },
    setActiveItemSidebar: (payload) => {
      dispatch(actionsUi.setActiveItemSidebar(payload));
    },
    setCurrentViewDashboardDisPatch: (payload) => {
      dispatch(actionsUi.setCurrentViewDashboard(payload));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
