import { Layout, Menu, Avatar } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { Redirect } from "react-router-dom";
import SiderBarComponent from "../../components/SiderBarComponent";
import * as actions from "../../actions/ui";
import SignInContainer from "../../containers/SignInContainer";

const { Header, Sider, Content } = Layout;
class SignIn extends React.Component {
  componentWillMount() {
    console.log("avoday");
    this.props.setActiveItemSidebar(2);
  }

  toggle = () => {
    this.props.setCollapsedSidebarDispatch(!this.props.collapsed);
  };

  render() {
    if (this.props.isLoggedIn) {
      return <Redirect to="/dashboard" />;
    }
    // const { history } = this.props;
    // if (this.props.isLoggedIn){
    //   history.push('/')
    // }
    return (
      <Layout>
        <SiderBarComponent {...this.props} />

        <Layout className="site-layout">
          <Header
            className="site-layout-background topbar-menu"
            style={{ padding: 0 }}
          >
            {React.createElement(
              this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: this.toggle,
              }
            )}
            <Avatar style={{ float: "right" }} src="./logo192.png" />
          </Header>
          <SignInContainer />
        </Layout>
      </Layout>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    collapsed: state.ui.collapsedSideBar,
    activeItemSideBar: state.ui.activeItemSideBar,
    isLoggedIn: state.user.isLoggedIn,
    userData: state.user.userData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCollapsedSidebarDispatch: (payload) => {
      dispatch(actions.setCollapsedSidebar(payload));
    },
    setActiveItemSidebar: (payload) => {
      dispatch(actions.setActiveItemSidebar(payload));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
